<?php
##########################################################################
# Copyright (C) 2024 - contributors of the "madbob thanks generator"
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
##########################################################################

// asd.
$COWSAY = '/usr/games/cowsay';

// Input phrase.
$ASD = trim(file_get_contents('thanks.txt'));

// Parse "cowsay -l" output to get available cows.
$cows = [];
exec( sprintf(
	"%s -l",
	escapeshellarg( $COWSAY )
), $cows_list_lines );
array_shift($cows_list_lines);
foreach( $cows_list_lines as $cows_list_line ) {
	$cows_list_line_parts = explode( " ", $cows_list_line );
	foreach( $cows_list_line_parts as $cows_list_line_part ) {
		$cows_list_line_part = trim($cows_list_line_part);
		if ($cows_list_line_part !== '') {
			$cows[] = $cows_list_line_part;
		}
	}
}

// Take the cowsay template name.
$input_cow = null;
$input_cow_raw = $_GET['cow'] ?? null;
if (is_string($input_cow_raw) && in_array($input_cow_raw, $cows)) {
	$input_cow = $input_cow_raw;
}

$next = (int)($_GET['next'] ?? 0);
$next = $next % count($cows);
if (!$input_cow) {
	$input_cow = $cows[$next];
}

$next++;

$command = sprintf(
	"%s -W 60 -f %s",
	escapeshellcmd( $COWSAY ),
	escapeshellarg( $input_cow )
);

?><!DOCTYPE html>
<head>
<title>Viva madbob! Di NO al software Proprietario</title>
<style>
body {
	width: 100%;
	max-width: 800px;
	min-width: 500px;
	margin: auto;
	padding: 0.5em 1em;
}
pre {
	border-left: 2px dashed lightgray;
	padding-left: 1em;
}
</style>
</head>
<body>
<h1>Viva madbob! Di NO al software Proprietario</h1>
<p><a href="/?next=<?= htmlspecialchars( $next ) ?>">Clicca qui se non sei un robot</a></p>
<hr />
<pre>$ <?= htmlentities( $command ) ?></pre>
<pre><?=
	htmlentities( shell_exec( sprintf(
		"%s -- %s",
		$command,
		escapeshellarg( $ASD )
	) ) );
?></pre>
<hr />
<p>Crediti:</p>

<ul>
	<li><a href="https://www.madbob.org/">madbob</a> è hardware rilasciato in licenza di software libero.</a></li>
	<li><a href="https://packages.debian.org/stable/cowsay">cowsay</a> è un software realizzato da Tony Monroe e rilasciato in licenza libera comprendente varie mucche e cose amene in ASCII art, di cui alcune probabilmente dalla bellezza ampiamente discutibile. Contattare i maintainer in caso di disagio con alcune ASCII-art particolarmente sciocche. Qui ci limitiamo a mostrare tutte quelle disponibili nel pacchetto Debian di cowsay. Letteralmente, le mostriamo tutte. Quindi speriamo che il pacchetto sia aggiornato, altrimenti saremo persi.</li>
	<li><a href="https://www.debian.org/">Debian GNU/Linux</a> è il sistema operativo universale, anche per lanciare mucche. E' rilasciato in licenza libera.</li>
	<li><a href="https://packages.debian.org/stable/php">PHP</a> è un pre-processore di ipertesti che in 15 righe tiene online questa pagina ed è rilasciato in licenza libera.</li>
	<li><a href="https://packages.debian.org/stable/certbot">certbot</a> è un pezzo di software per i certificati Let's Encrypt usati in questa pagina ed è rilasciato in licenza libera.</li>
	<li><a href="https://packages.debian.org/stable/nano">GNU nano</a> è un potente ambiente di sviluppo completo con cui scrivere siti web portentosi come questo. USATELO ORA ACCIPIGNA E' PORTENTOSO.</li>
	<li>Questo sito è orgogliosamente senza manco una riga di JavaScript ed è quindi totalmente compliant con <a href="https://www.gnu.org/software/librejs/index.html">LibreJS</a>. Forse questo e' anche l'unico modo per essere compliant.</li>
	<li>Questo sito è a sua volta software libero, con una licenza super-libera che mai ti permetterà di introdurre software proprietario! MUAHAHAHAH! Puoi farci qualsiasi cosa, anche i miliardi, ma mai, mai, potrai introdurre software proprietario.</li>
</ul>
</body>
</html><?php
